1. What is an alternative term for pinging?
Answer: Monitoring (from instance summary)

2. What is the term used when a server cannot be pinged?
Answer: Server is down

3. A security setting that limits the communication of systems from outside the server.
Answer: firewall

4. A security setting that limits the communication of systems from within the server.
Answer: firewall

5. From which tab in the instance information summary can we change the firewall rules of the instance?
Answer: Security

6. ICMP stands for?
Answer: Internet Control Message Protocol

7. TCP stands for?
Answer: Transmission Control Protocol

8. Which protocol does a ping request use?
Answer: Internet Control Message Protocol (ICMP)

9. What is the IP address that indicates access from anywhere?
Answer: IPv4

10. A command that can be used to look on various usage metrics for a given server.
Answer: htop

11. Processor or memory __________ means the amount of resources needed to keep a given task or process running smoothly within the operating system.
Answer: management

12. A command that can be used to determine disk storage usage.
Answer: df

13. From which tab in the instance information summary can we view the usage of an instance without directly accessing the instance?
Answer: Monitoring

14. A command that can be used to edit the contents of a file.
Answer: echo

15. A command used to host a Node.js app.
Answer: npm

16. Which protocol is used to enable access to a Node.js app on a given port?
Answer: Transmission Control Protocol (TCP)